package com.catalogo;

import javax.swing.JFrame;

import com.catalogo.view.MenuView;

/**
 * @author silvionetto
 * 
 */
public class CatalogoApplication {

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {
	// Create and set up the window.
	MenuView view = new MenuView();

	// Create and set up the window.
	view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	// Display the window.
	view.pack();
	view.setVisible(true);
    }

    public static void main(String[] args) {
	// Schedule a job for the event-dispatching thread:
	// creating and showing this application's GUI.
	javax.swing.SwingUtilities.invokeLater(new Runnable() {
	    public void run() {
		createAndShowGUI();
	    }
	});
    }
}

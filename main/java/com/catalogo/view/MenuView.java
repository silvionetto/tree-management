package com.catalogo.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MenuView extends JFrame implements ActionListener {

    /** The Constant TITLE. */
    private static final String TITLE = "Catalogo";

    interface ActionName {
	String NEW = "New";
	String FIND = "Find";
    }

    JMenuBar menuBar;
    JMenu menu, submenu;
    JMenuItem menuItem;

    /**
     * Default Constructor.
     */
    public MenuView() {
	// Set title
	setTitle(TITLE);
	setMenu();
    }

    private void setMenu() {
	// Create the menu bar.
	menuBar = new JMenuBar();

	// Build the first menu.
	menu = new JMenu("Menu");
	menu.setMnemonic(KeyEvent.VK_M);
	menu.addActionListener(this);
	menuBar.add(menu);

	// a group of JMenuItems
	menuItem = new JMenuItem(ActionName.NEW, KeyEvent.VK_N);
	menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
		ActionEvent.ALT_MASK));
	menuItem.addActionListener(this);
	menu.add(menuItem);

	menuItem = new JMenuItem(ActionName.FIND, KeyEvent.VK_F);
	menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,
		ActionEvent.ALT_MASK));
	menuItem.setMnemonic(KeyEvent.VK_F);
	menuItem.addActionListener(this);
	menu.add(menuItem);

	setJMenuBar(menuBar);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
	if (ActionName.NEW.equals(e.getActionCommand())) {
	    CatalogoView view = new CatalogoView();
	    view.pack();
	    view.setVisible(true);
	} else if (ActionName.FIND.equals(e.getActionCommand())) {
	    System.out.println(ActionName.FIND);
	}
    }
}

package com.catalogo.view;

import java.awt.BorderLayout;
import java.awt.Button;

import javax.swing.JFrame;
import javax.swing.JTextField;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class CatalogoView.
 * 
 * @author silvionetto
 */
public class CatalogoView extends JFrame {

    /** The Constant TITLE. */
    private static final String TITLE = "Catalogo";

    private final JTextField gps = new JTextField();
    private final JTextField numeroArvore = new JTextField();
    private final JTextField especie = new JTextField();
    private final JTextField cap = new JTextField();
    private final JTextField h = new JTextField();
    private final Button saveButton = new Button("Save");
    private final Button newButton = new Button("New");
    private final Button deleteButton = new Button("Delete");

    /**
     * Default Constructor.
     */
    public CatalogoView() {
	// Set title
	setTitle(TITLE);
	initializeLayout();
    }

    /**
     * Initialize layout.
     */
    private void initializeLayout() {
	String colunas = "20dlu,10dlu,60dlu,10dlu,100dlu,10dlu,20dlu,10dlu,20dlu";
	String linhas = "pref,2dlu,pref,2dlu,pref";

	FormLayout layout = new FormLayout(colunas, linhas);
	PanelBuilder builder = new PanelBuilder(layout);
	builder.setDefaultDialogBorder();

	builder.addLabel("GPS", CC.xy(1, 1));
	builder.add(gps, CC.xy(1, 3));

	builder.addLabel("Número da Árvore", CC.xy(3, 1));
	builder.add(numeroArvore, CC.xy(3, 3));

	builder.addLabel("Espécie", CC.xy(5, 1));
	builder.add(especie, CC.xy(5, 3));

	builder.addLabel("CAP", CC.xy(7, 1));
	builder.add(cap, CC.xy(7, 3));

	builder.addLabel("H", CC.xy(9, 1));
	builder.add(h, CC.xy(9, 3));

	builder.add(newButton, CC.xy(1, 5));
	builder.add(saveButton, CC.xy(3, 5));
	builder.add(deleteButton, CC.xy(5, 5));

	getContentPane().setLayout(new BorderLayout());
	getContentPane().add(builder.getPanel(), BorderLayout.CENTER);
    }
}